import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Table,
  Button,
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  ModalFooter,
} from "reactstrap";
import { SegmentoService } from "./service/SegmentoService";

const data = [
 
]

class Formulario extends React.Component{
    state = {
        data: data,
        modalActualizar: false,
        modalAgregar: false,
        form: {
            id: "",
            longitud: "",
            via: "",
            estrato: ""
        }
    }
    segmentoService 
constructor(){
    super();
    this.segmentoService = new SegmentoService();
    
  }

 componentDidMount(){
    this.segmentoService.mostrar().then(data => this.setState({data : data}))

  }

    insertar = () => {

        var nuevoSegmento= {...this.state.form};
        this.segmentoService.save(nuevoSegmento).then(data => {
          var lista = this.state.data;
          lista.push(data);
          this.setState({modalInsertar: false, data: lista});
      });
       
    
    }
    
    mostrarModalInsertar = () => {
        this.setState({
            modalInsertar: true
        });
    };
    
    cerrarModalInsertar = () => {
        this.setState({
            modalInsertar: false
        });
    };
    
    editar = (dato) => {
        this.segmentoService.update(dato).then(dato =>{
          var cont = 0;
          var segmento = this.state.data;
          segmento.forEach((registro) => {
            if (dato.id === registro.id) {
              segmento[cont].longitud = dato.longitud;
              segmento[cont].via = dato.via;
              segmento[cont].estrato = dato.estrato;
            }
            cont++;
          });
          this.setState({ data: segmento, mostrarModalActualizar: false});
      
        });
      
        
      };
    
    cerrarModalActualizar = () => {
        this.setState({
            mostrarModalActualizar: false
        });
      };
    
      handleChange = (e) => {
        this.setState({
          form: {
            ...this.state.form,
            [e.target.name]: e.target.value,
          },
        });
      };
    
    eliminar = (dato) => {
      var opcion = window.confirm('¿Está seguro que desea eliminar este segmento?')
      if (opcion === true) {
        this.segmentoService.delete(dato).then(()=>{
          var cont = 0;
          var segmento = this.state.data;
          segmento.forEach((registro) => {
              if (dato.id === registro.id) {
                  segmento.splice(cont, 1);
              }
              cont++;
          });
          this.setState({ data: segmento, modalActualizar: false})
        })
         
      }
      
       
    };
    
    mostrarModalActualizar = (registro) => {
        this.setState({
            form: registro,
            mostrarModalActualizar: true,
    
        })
      };
    render(){
        return(
            <>
            <Container>
                <br></br>
        <Button color="success"
        onClick={()=> this.mostrarModalInsertar()}>
        Agregar Segmento
        </Button>
        <br></br>
        <br></br>
        <Table>
            <thead>
                <tr>
                    <th>Longitud</th>
                    <th>Tipo de vía</th>
                    <th>Estrato</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody>
                {this.state.data.map((elemento)=>
                <tr key = {elemento.id}>
                    <td> {elemento.longitud} </td>
                    <td> {elemento.via} </td>
                    <td> {elemento.estrato} </td>
                    <td>
                        <Button
                        color="primary"
                        onClick={()=> this.mostrarModalActualizar(elemento)}
                        >Editar</Button>
                        {"  "}
                        <Button
                        color="danger"
                        onClick={()=> this.eliminar(elemento)}
                        >
                         Eliminar   
                        </Button>
                    </td>
                </tr>
                )}
            </tbody>
        </Table>
    </Container>
	<Modal isOpen={this.state.mostrarModalActualizar}>  
		<ModalHeader>
        <div><h3>Editar Registro</h3></div>
        </ModalHeader>
        <ModalBody>
        <FormGroup>
              <label>
                ID: 
              </label>
              <input
                className="form-control"
                readOnly
                type="text"
                value={this.state.form.id}
              />
        </FormGroup>
  <FormGroup>
    <label>
    Longitud:
    </label>
    <input className="form-control"
    name="longitud"
    type="text"
    onChange= {this.handleChange}
    value= {this.state.form.longitud}></input>
</FormGroup>
<FormGroup>
    <label>
        Tipo de vía:
    </label>
    <select value={this.state.form.via} onChange={this.handleChange}
    name="via">"
        <option></option>
        <option name="Autopista">Autopista</option>
        <option name="Avenida">Avenida</option>
        <option name="Calle">Calle</option>
        <option name="Carrera">Carrera</option>
        <option name="Diagonal">Diagonal</option>
    </select>
</FormGroup>
<FormGroup>
    <label>
        Estrato:
    </label>
    <select value= {this.state.form.estrato} onChange= {this.handleChange}
    name="estrato">
        <option></option>
        <option name="0">0</option>
        <option name="1">1</option>
        <option name="2">2</option>
        <option name="3">3</option>
        <option name="4">4</option>
        <option name="5">5</option>
        <option name="6">6</option>
    </select>
</FormGroup>
        </ModalBody>

<ModalFooter>
    <Button
      color="primary"
      onClick={()=> this.editar(this.state.form)}>Guardar Cambios
    </Button>
    <Button 
    className= "btn btn-danger"
    onClick={() => this.cerrarModalActualizar()}>
    Cancelar
    </Button>
</ModalFooter>
	</Modal>
<Modal isOpen={this.state.modalInsertar}>
<ModalHeader>
<div><h3>Agregar Segmento</h3></div>
</ModalHeader>
        <ModalBody>
        <FormGroup>
              <label>
                ID: 
              </label>
              <input
                className="form-control"
                readOnly
                type="text"
                
              />
        </FormGroup>
        <FormGroup>
            <label>
            Longitud:
            </label>
            <input className="form-control"
            name="longitud"
            type="text"
            onChange= {this.handleChange}
            value= {this.state.longitud}></input>
        </FormGroup>
        <FormGroup>
            <label>
                Tipo de vía:
            </label>
            <select value={this.state.via} onChange={this.handleChange}
            name="via">"
                <option></option>
                <option name="Autopista">Autopista</option>
                <option name="Avenida">Avenida</option>
                <option name="Calle">Calle</option>
                <option name="Carrera">Carrera</option>
                <option name="Diagonal">Diagonal</option>
            </select>
        </FormGroup>
        <FormGroup>
            <label>
                Estrato:
            </label>
            <select value= {this.state.estrato} onChange= {this.handleChange}
            name="estrato">
                <option></option>
                <option name="0">0</option>
                <option name="1">1</option>
                <option name="2">2</option>
                <option name="3">3</option>
                <option name="4">4</option>
                <option name="5">5</option>
                <option name="6">6</option>
            </select>
        </FormGroup>
        </ModalBody>
           <ModalFooter>
           <Button
            color="primary"
            onClick= {()=> this.insertar()}> Agregar</Button>
            <Button
            className="btn btn-danger"
            onClick= {() => this.cerrarModalInsertar()}> Cancelar</Button>

           </ModalFooter>
    </Modal>
            </>
        )
    }
}
export default Formulario;



