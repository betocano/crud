import Axios from 'axios';

export class SegmentoService {
    baseUrl = "http://localhost:9090/segmento/"
    mostrar(){
        return Axios.get(this.baseUrl + 'mostrar').then(res => res.data);
        
    }

    save(segmento){
        return Axios.post(this.baseUrl + 'agregar', segmento).then(res => res.data);
    }

    update(segmento){
        return Axios.put(this.baseUrl + 'actualizar', segmento).then(res => res.data);
    }

    delete(segmento){
        return Axios.delete(this.baseUrl + 'eliminar/' + segmento.id).then(res => res.data);
    }
}